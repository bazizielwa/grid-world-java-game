package fr.GAME4J;

import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.ObjectOutputStream;
import java.io.IOException;

import java.time.LocalDate;
import java.time.LocalTime;

public class Serialiser {

    public static void main(String[] args) {

        //Creation Map et Avatar
        Map map = new Map(10, 10);
        Avatar j1 = new Avatar(50, 5, map);

        //Creation Date et heure
        LocalDate date_of_today = LocalDate.now();
        LocalTime time1 = LocalTime.now();
        LocalTime time2 = LocalTime.now();

        //Creation Game comprenant la Map et Avatar
        Game game = new Game(map,j1,date_of_today,time1,time2);

        try {


            // serialisation de la map
            FileOutputStream fos1 = new FileOutputStream("H:\\Documents\\Java\\new\\src\\texte\\map.txt");
            ObjectOutputStream os1 = new ObjectOutputStream(fos1);
            os1.writeObject(map);
            System.out.println("Objet map sÃ©rialisÃ©");
            os1.close();
            System.out.println("Fermeture");

            // serialisation de l'avatar j1
            FileOutputStream fos2 = new FileOutputStream("H:\\Documents\\Java\\new\\src\\texte\\j1.txt");
            ObjectOutputStream os2 = new ObjectOutputStream(fos2);
            os2.writeObject(j1);
            System.out.println("Objet j1 sÃ©rialisÃ©");
            os2.close();
            System.out.println("Fermeture");

            // serialisation de la game
            FileOutputStream fos3 = new FileOutputStream("H:\\Documents\\Java\\new\\src\\texte\\game.txt");
            ObjectOutputStream os3 = new ObjectOutputStream(fos3);
            os3.writeObject(game);
            System.out.println("Objet game sÃ©rialisÃ©");
            os3.close();
            System.out.println("Fermeture");
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }

}
