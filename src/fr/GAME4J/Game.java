package fr.GAME4J;

import com.sun.xml.internal.bind.v2.runtime.output.StAXExStreamWriterOutput;

import java.lang.String;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;

import java.io.Serializable;

public class Game implements Serializable {

    //private static final long serialVersionUID = - 985974763921291337L;

    public int date;
    public int hour;
    private Map map;
    private Case case1;
    private Avatar avatar1;
    boolean ragequit = false;

    public Game(Map map1, Avatar avatar, LocalDate date_of_today, LocalTime time1, LocalTime time2) {
        map = map1;
        avatar1 = avatar;
    }

    public char addCase(int number, String nature) {
        Random x = new Random();
        Random y = new Random();

        if (nature == "obstacle") {
            Obstacle obstacle = new Obstacle(x.nextInt(map.getLength()), y.nextInt(map.getWidth()), number);
            return map.setGrid(obstacle, 'O');
        }

        if (nature == "food") {
            Food food = new Food(x.nextInt(map.getLength()), y.nextInt(map.getWidth()), number);
            return map.setGrid(food, 'F');
        }
        if (nature == "starting point") {
            Case start = new Case(0, 0, 0);
            avatar1.moveAvatar.add(start);
            return map.setGrid(start, 'A');
        }
        if (nature == "house") {
            Case house = new Case(map.getLength() - 1, map.getWidth() - 1, 0);
            return map.setGrid(house, 'H');
        }
        else
            System.out.println("Impossible de cree cette case");

        return 0;

    }
    public char removeCase(int i,int j) {
        return map.setGrid(i,j, '.');
    }

    public void playMove() {
        try {
            int i = 0, dir;
            if (avatar1.getEnergyON() > 0) {
                while (avatar1.getEnergyON() > 0 && map.grid[map.getLength() - 1][map.getWidth() - 1] == 'H') {
                    System.out.println("Enter direction :");
                    Scanner sc = new Scanner(System.in);
                    dir = sc.nextInt();
                    switch (dir) {
                        case 8:
                            avatar1.move('N');
                            break;
                        case 4:
                            avatar1.move('W');
                            break;
                        case 6:
                            avatar1.move('E');
                            break;
                        case 2:
                            avatar1.move('S');
                            break;
                        case 5:
                            avatar1.backMove();
                            break;
                        case 0:
                            ragequit = true;
                            System.out.println("You just ragequit my friend");
                            //Insert.UpdateTime2();
                            //Insert.DisplayDG2();
                            return;

                        default:
                    }
                    i++;
                }
            }
            if (avatar1.getEnergyON() <= 0) {
                System.out.println("GAME OVER !!!");
                return;
            }
        }catch (InputMismatchException e){
            System.out.println("----------------------------------------------");
            System.out.println("Enter a number pls !");
            playMove();
        }}

    public static void main(String[] args) {
//1ere Game
        System.out.println("----------------------GAME4J-------------------");
        //Creation Map et Avatar
        Map map = new Map(10, 10);
        Avatar j1 = new Avatar(50, 5, map);

        //Creation Date et heure
        LocalDate date_of_today = LocalDate.now();
        LocalTime time1 = LocalTime.now();
        LocalTime time2 = LocalTime.now();

        //Creation Game comprenant la Map et Avatar
        Game game = new Game(map,j1,date_of_today,time1,time2);

        //Creation des Case
        for (int i = 0; i < 10; i++) {
            game.addCase(0, "food");
            game.addCase(2, "obstacle");
        }
        game.addCase(5,"starting point");
        game.addCase(5,"house");

        /*try {
            // 1 - charger le driver mysql
            Class.forName("com.mysql.jdbc.Driver");
            // 2 - créer la connexion
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3308/game_database", "root","");
            // 3 - créer un état de connexion
            Statement st = con.createStatement();
            // 4 - créer une requête de sélection
            ResultSet res = st.executeQuery("SELECT * FROM games_dates");
            // 5 - parcourt des données
            //DateTime(2);
            while(res.next()){
                System.out.println("Date de la partie interrompue : " + res.getDate(2));
                System.out.println("Heure de fin de jeu : " + res.getTime(3));
                System.out.println("--------------------------------------------------------");
            }

        }

        catch (Exception e){
            System.out.println("ERROR :" + e.getMessage());
        }*/

        // ajouter une partie et ses données dans la bdd
        //Insert.DataGame();

        // afficher date et heure de la partie lancée dans la console
        //Insert.DisplayDG1();

        map.display();

        game.playMove();

//Restauration de la game interrompu par l'utilisateur
        while (game.ragequit==true){
            Scanner n3 = new Scanner(System.in);
            System.out.println("Do you want to get back into you're cancelled game ? yes or no :");
            String resp3 = n3.next();
            if (resp3.compareTo("yes") == 0) {
                System.out.println("---Reload Game---");
                map.display();
                game.playMove();
                if (j1.winOrNot()==true){
                    game.ragequit=false;
                }
            }
        else game.ragequit=false;}

//Affichage du replay 1ere Game
        if (j1.winOrNot()!=false){
            Scanner n1 = new Scanner(System.in);
            System.out.println("Do you want to see a replay of this game? yes or no ? :");
            String resp1 = n1.next();
            Map map3 = new Map(10, 10);
            if (resp1.compareTo("yes")==0) {
                System.out.println("-------------------Here the path-------------------");
                for (int i = 0; i < j1.moveAvatar.size(); i++) {
                    int xb = j1.moveAvatar.get(i).getCoordinateX();
                    int yb = j1.moveAvatar.get(i).getCoordinateY();
                    map3.setGrid(xb, yb, 'A');
                }
                map3.display();
            }
        }else System.out.println("Good bye");


//2eme Game
        Scanner n = new Scanner(System.in);
        System.out.println("Do you want to play more ? yes or no ? :");
        String resp = n.next();
        if (resp.compareTo("yes")==0){
            System.out.println("----------------------GAME4J-------------------");

            Map map1 = new Map(10, 10);
            Avatar j2 = new Avatar(50, 5, map1);
            Game game1 = new Game(map1,j2,date_of_today,time1,time2);

            for (int i = 0; i < 10; i++) {
                game1.addCase(0, "food");
                game1.addCase(2, "obstacle");
            }
            game1.addCase(5,"starting point");
            game1.addCase(5,"house");

            map1.display();
            game1.playMove();

//Restauration de la game interrompu par l'utilisateur
            if (game1.ragequit==true){
                Scanner n3 = new Scanner(System.in);
                System.out.println("Do you want to get back into you're cancelled game ? yes or no :");
                String resp3 = n3.next();
                if (resp3.compareTo("yes") == 0) {
                    System.out.println("-------------------Reload Game-------------------");
                    map1.display();
                    game1.playMove();
                }if (j2.winOrNot()==true){
                    game1.ragequit=false;
                }
                else game1.ragequit=false;
            }

//Affichage replay 2eme Game
            if (j2.winOrNot()!=false) {
                Scanner n2 = new Scanner(System.in);
                System.out.println("Do you want to see a replay of this game? yes or no ? :");
                String resp2 = n2.next();
                Map map4 = new Map(10, 10);
                if (resp2.compareTo("yes") == 0) {
                    System.out.println("-------------------Here the path-------------------");
                    for (int i = 0; i < j2.moveAvatar.size(); i++) {
                        int xb = j2.moveAvatar.get(i).getCoordinateX();
                        int yb = j2.moveAvatar.get(i).getCoordinateY();
                        map4.setGrid(xb, yb, 'A');
                    }
                    map4.display();
                } else System.out.println("Good bye");
            }}



    }}
