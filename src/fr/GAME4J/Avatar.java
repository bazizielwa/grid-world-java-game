package fr.GAME4J;

import java.awt.event.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Avatar implements Serializable {

    //private static final long serialVersionUID = - 985974763921291337L;

    private int energyInit;
    private int energyON;
    public int nb_annulationMax;
    public int nb_annulationOn;
    List<Case> moveAvatar;
    List<Case> histoFood;
    Map map1;
    Game game;
    Case case1;

    public Avatar(int energyInit, int nb_annulationMax, Map map2) {
        this.energyInit = energyInit;
        this.nb_annulationMax = nb_annulationMax;
        this.energyON = energyInit;
        map1 = map2;
        moveAvatar = new ArrayList<>();
        histoFood = new ArrayList<>();
    }

    public int getEnergyInit() {
        return energyInit;
    }

    public void setEnergyInit(int energyInit) {
        this.energyInit = energyInit;
    }

    public int getEnergyON() {
        return energyON;
    }

    public void setEnergyON(int energy) {
        this.energyON = energyON + energy;
    }

    public int getNb_annulationMax() {
        return nb_annulationMax;
    }

    public void setNb_annulationMax(int nb_annulationMax) {
        this.nb_annulationMax = nb_annulationMax;
    }

    public int getNb_annulationOn() {
        return nb_annulationOn;
    }

    public void setNb_annulationOn(int nb) {
        this.nb_annulationOn = nb_annulationOn+nb;
    }

    @Override
    public String toString() {
        return "Avatar{" +
                "energyInit=" + energyInit +
                ", energyON=" + energyON +
                ", nb_annulationMax=" + nb_annulationMax +
                ", nb_annulationOn=" + nb_annulationOn +
                "}, Current " + moveAvatar.get(moveAvatar.size()-1);
    }

    public void move(char direction) {
        char tmp;
        for (int i = 0; i < map1.getLength(); i++) {
            for (int j = 0; j < map1.getWidth(); j++) {
                //NORTH
                if (direction == 'N' && map1.grid[i][j] == 'A') {
                        //Case food
                        if (i - 1 >= 0 && map1.grid[i - 1][j] == 'F') {
                            tmp = map1.grid[i][j];
                            map1.setGrid(i, j, '.');
                            map1.setGrid(i - 1, j, tmp);
                            Case case1 = new Case(i - 1, j, (i + j / 2));
                            moveAvatar.add(case1);
                            histoFood.add(new Food(i - 1, j, (i + j / 2)));
                            setEnergyON(9);
                            warningBack();
                        }
                        //Case empty
                        if (i - 1 >= 0 && map1.grid[i - 1][j] == '.') {
                            tmp = map1.grid[i][j];
                            map1.setGrid(i, j, '.');
                            map1.setGrid(i - 1, j, tmp);
                            Case case1 = new Case(i - 1, j, (i + j / 2));
                            moveAvatar.add(case1);
                            setEnergyON(-1);
                            warningBack();
                        }
                        //Case obstacle
                        if (i - 1 >= 0 && map1.grid[i - 1][j] == 'O') {
                            setEnergyON(-10);
                            warningBack();
                        }
                }//SUD
                if (direction == 'S' && map1.grid[i][j] == 'A') {
                        //Case empty
                        if (i + 1 < map1.getLength() && map1.grid[i + 1][j] == '.') {
                            tmp = map1.grid[i][j];
                            map1.setGrid(i, j, '.');
                            map1.setGrid(i + 1, j, tmp);
                            setEnergyON(-1);
                            Case case1 = new Case(i + 1, j, (i + j / 2));
                            moveAvatar.add(case1);
                            warningBack();
                            System.out.println(this.toString());
                            map1.display();
                            return;
                        }
                    //Case final house
                    if (i + 1 < map1.getLength() && map1.grid[i + 1][j] == 'H') {
                        tmp = map1.grid[i][j];
                        map1.setGrid(i, j, '.');
                        map1.setGrid(i + 1, j, tmp);
                        setEnergyON(-1);
                        Case case1 = new Case(i + 1, j, (i + j / 2));
                        moveAvatar.add(case1);
                        warningBack();
                        System.out.println(this.toString());
                        map1.display();
                        System.out.println();
                        if (winOrNot()==true){
                            System.out.println("GAME WIN !!!");
                        }
                        return;
                    }
                        //Case food
                        if (i + 1 < map1.getLength() && map1.grid[i + 1][j] == 'F') {
                            tmp = map1.grid[i][j];
                            map1.setGrid(i, j, '.');
                            map1.setGrid(i + 1, j, tmp);
                            setEnergyON(9);
                            Case case1 = new Case(i + 1, j, (i + j / 2));
                            moveAvatar.add(case1);
                            histoFood.add(new Food(i + 1, j, (i + j / 2)));
                            warningBack();
                            System.out.println(this.toString());
                            map1.display();
                            return;
                        }
                        //Case obstacle
                        if (i + 1 < map1.getLength() && map1.grid[i + 1][j] == 'O') {
                            setEnergyON(-10);
                            warningBack();
                            System.out.println(this.toString());
                            map1.display();
                            return;
                        }
                }
                //EST
                if (direction == 'E') {
                    if (map1.grid[i][j] == 'A') {
                        //Case food
                        if (j + 1 < map1.getWidth() && map1.grid[i][j + 1] == 'F') {
                            tmp = map1.grid[i][j];
                            map1.setGrid(i, j, '.');
                            map1.setGrid(i, j + 1, tmp);
                            Case case1 = new Case(i,j+1,(i + j / 2));
                            moveAvatar.add(case1); //Ajout a la liste des deplacements
                            histoFood.add(new Food(i, j + 1, (i + j / 2)));//Ajout a la liste des deplacements
                            setEnergyON(9);
                            warningBack();
                            System.out.println(this.toString());
                            map1.display();
                            return;
                        }
                        //Case empty
                        if (j + 1 < map1.getWidth() && map1.grid[i][j + 1] == '.') {
                            tmp = map1.grid[i][j];
                            map1.setGrid(i, j, '.');
                            map1.setGrid(i, j + 1, tmp);
                            Case case1 = new Case(i,j+1,(i + j / 2));
                            moveAvatar.add(case1); //Ajout a la liste des deplacements
                            setEnergyON(-1);
                            warningBack();
                            System.out.println(this.toString());
                            map1.display();
                            return;
                        }
                        //Case final house
                        if (j + 1 < map1.getWidth() && map1.grid[i][j + 1] == 'H') {
                            tmp = map1.grid[i][j];
                            map1.setGrid(i, j, '.');
                            map1.setGrid(i, j + 1, tmp);
                            Case case1 = new Case(i,j+1,(i + j / 2));
                            moveAvatar.add(case1); //Ajout a la liste des deplacements
                            setEnergyON(-1);
                            warningBack();
                            System.out.println(this.toString());
                            map1.display();
                            if (winOrNot()==true){
                                System.out.println("GAME WIN !!!");
                            }
                            return;
                        }
                        //Case obstacle
                        if (j + 1 < map1.getWidth() && map1.grid[i][j + 1] == 'O') {
                            setEnergyON(-10);
                            warningBack();
                            System.out.println(this.toString());
                            map1.display();
                            return;
                        }
                    }
                }
                //WEST
                if (direction == 'W' && map1.grid[i][j] == 'A') {
                        //Case food
                        if (j - 1 >= 0 && map1.grid[i][j - 1] == 'F') {
                                tmp = map1.grid[i][j];
                                map1.setGrid(i, j, '.');
                                map1.setGrid(i, j - 1, tmp);
                                Case case1 = new Case(i, j - 1, (i + j / 2));
                                moveAvatar.add(case1);
                                histoFood.add(new Food(i, j - 1, (i + j / 2)));
                                setEnergyON(9);
                                warningBack();
                            }
                        //Case empty
                            if (j - 1 >=0 && map1.grid[i][j - 1] == '.') {
                                    tmp = map1.grid[i][j];
                                    map1.setGrid(i, j, '.');
                                    map1.setGrid(i, j - 1, tmp);
                                    Case case1 = new Case(i, j - 1, (i + j / 2));
                                    moveAvatar.add(case1);
                                    setEnergyON(-1);
                                    warningBack();
                            }
                        //Case obstacle
                            if (j - 1 >= 0 && map1.grid[i][j - 1] == 'O') {
                                setEnergyON(-10);
                                warningBack();
                            }
                        }
                }
        }
        System.out.println(this.toString());
        map1.display();
    }

    public void backMove(){
        if (nb_annulationOn<5){
        for (int i = 0; i < map1.getLength(); i++) {
            for (int j = 0; j < map1.getWidth(); j++) {
                if (i-1>=0 || j-1>=0 && j <map1.getWidth()-1 && i<map1.getLength()-1){
                if(histoFood.size()>0){
                    if (map1.grid[i][j] == 'A' && moveAvatar.get(moveAvatar.size()-1).getCoordinateX()==histoFood.get(histoFood.size()-1).getCoordinateX() && moveAvatar.get(moveAvatar.size()-1).getCoordinateY()==histoFood.get(histoFood.size()-1).getCoordinateY()) {
                        //Recuperation de l'ancienne case en remettant l'ancienne
                        int xb =moveAvatar.get(moveAvatar.size()-2).getCoordinateX();
                        int yb =moveAvatar.get(moveAvatar.size()-2).getCoordinateY();
                        map1.setGrid(xb, yb, 'A');
                        map1.setGrid(i,j, 'F');
                        moveAvatar.remove(moveAvatar.size()-1);
                        histoFood.remove(histoFood.size()-1);
                        setNb_annulationOn(1);
                        setEnergyON(-10);
                    }
                    if (map1.grid[i][j] == 'A') {
                        if(moveAvatar.get(moveAvatar.size()-1).getCoordinateX()!=histoFood.get(histoFood.size()-1).getCoordinateX() || moveAvatar.get(moveAvatar.size()-1).getCoordinateY()!=histoFood.get(histoFood.size()-1).getCoordinateY()){
                        //Recuperation de l'ancienne case cas ou l'on est pas sur une case food (liste<Food> size >0)
                        int xb =moveAvatar.get(moveAvatar.size()-2).getCoordinateX();
                        int yb =moveAvatar.get(moveAvatar.size()-2).getCoordinateY();
                        map1.setGrid(xb, yb, 'A');
                        map1.setGrid(i,j, '.');
                        moveAvatar.remove(moveAvatar.size()-1);
                        setNb_annulationOn(1);
                        setEnergyON(1);
                    }
                    }
                }
                else if (map1.grid[i][j] == 'A') {
                    //Recuperation de l'ancienne case dans le cas ou la liste food est vide
                    int xb =moveAvatar.get(moveAvatar.size()-2).getCoordinateX();
                    int yb =moveAvatar.get(moveAvatar.size()-2).getCoordinateY();
                    map1.setGrid(xb, yb, 'A');
                    map1.setGrid(i,j, '.');
                    moveAvatar.remove(moveAvatar.size()-1);
                    setNb_annulationOn(1);
                    setEnergyON(1);
                }

            }
            }
        }
        System.out.println(this.toString());
        map1.display();}
        else
            System.out.println("You can't get back more !!");
    }

    public boolean warningBack() {
        for (int i = 0; i < moveAvatar.size() - 1; i++) {
            if (moveAvatar.get(i).equals(moveAvatar.get(moveAvatar.size() - 1))) {
                System.out.println("\"WARNING !!! You have already traveled this box\"");
                return true;
            }
        }
        return false;
    }

    public boolean winOrNot(){
        for (int i = 0; i < moveAvatar.size() - 1; i++) {
            for (int j = 0; j < moveAvatar.size() - 1; j++) {
                if (i == map1.getLength() - 1 && j == map1.getWidth() - 1) {
                    if (map1.grid[i][j] == 'A') {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

