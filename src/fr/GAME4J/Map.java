package fr.GAME4J;

import java.io.Serializable;

public class Map implements Serializable {

    //private static final long serialVersionUID = - 985974763921291337L;

    private int length;
    private int width;
    public char[][] grid;
    private Game game;
    private Avatar avatar;
    private Case case1;

    public Map(int length, int width) {
        this.length = length;
        this.width = width;
        this.grid = new char[length][width];

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                grid[i][j] = '.';
            }
        }
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public char setGrid(Case case1, char nature) {
        return grid[case1.getCoordinateX()][case1.getCoordinateY()] = nature;
    }

    public char setGrid(int x, int y, char nature) {
        return grid[x][y] = nature;
    }

    public int generateMatrix_distance() {
        return 0;
    }

    public int generateMatrix_energy() {
        return 0;
    }

    public void display() {
        System.out.println("O => Obstacle");
        System.out.println("F => Food");
        System.out.println("A => Avatar");
        System.out.println();
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print(" | " + grid[i][j]);
            }
            System.out.println(" | ");
        }
        System.out.println("----------------------------------------------");
    }
}
