package fr.GAME4J;

public class Case {

    private int coordinateX;
    private int coordinateY;

    public Case(int coordinateX, int coordinateY, int number) {

        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void getS() {

    }

    public void getN() {

    }

    public void getE() {

    }

    public void getW() {

    }

    public int getDistance() {
        return 0;
    }

    @Override
    public String toString() {
        return "Case{" +
                "coordinateX=" + coordinateX +
                ", coordinateY=" + coordinateY +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Case aCase = (Case) o;
        return coordinateX == aCase.coordinateX && coordinateY == aCase.coordinateY;
    }

}
