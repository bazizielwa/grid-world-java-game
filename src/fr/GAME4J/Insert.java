package fr.GAME4J;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;


public class Insert {

    public static boolean UpdateTime2(){ // met Ã  jour les donnÃ©es de la partie dans la bdd par exemple pour l'heure de fin de partie

        //create an object for time

        LocalTime time2 = LocalTime.now();

        try{
            Class.forName("com.mysql.jdbc.Driver");

            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3308/game_database", "root","");

            Statement st = con.createStatement();

            String query = "UPDATE games_dates SET end_time = '" + time2 + "'  ORDER BY id_game DESC LIMIT 1";
            st.executeUpdate(query);

            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            System.out.println();
        }
    }

    public static boolean DataGame(){ // insert les donnÃ©es d'une partie dans la bdd

        //create an object for date
        LocalDate date_of_today = LocalDate.now();
        LocalTime time1 = LocalTime.now();
        LocalTime time2 = LocalTime.now();


        try{
            Class.forName("com.mysql.jdbc.Driver");

            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3308/game_database", "root","");

            Statement st = con.createStatement();

            String query = "INSERT INTO games_dates (`date`, `start_time`, `end_time`) VALUES ('" + date_of_today + "', '" + time1 + "', '" + time2 + "') ";
            st.executeUpdate(query);


            return true;

        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            System.out.println("Une partie et ses donnÃ©es ont Ã©tÃ© ajoutÃ©s dans la BDD.");
        }
    }

    public static boolean DisplayDG1(){ // affiche les donnÃ©es d'une partie dans la console


        try{
            Class.forName("com.mysql.jdbc.Driver");

            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3308/game_database", "root","");

            Statement st = con.createStatement();

            ResultSet res2 = st.executeQuery("SELECT * FROM games_dates WHERE id_game = (SELECT max(id_game) FROM games_dates)");

            while(res2.next()){
                System.out.println("Date de la partie : " + res2.getDate(2));
                System.out.println("Heure du dÃ©but de la partie : " + res2.getTime(3));
            }

            return true;

        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            System.out.println();
        }
    }

    public static boolean DisplayDG2(){ // affiche les donnÃ©es d'une partie stoppÃ©e dans la console


        try{
            Class.forName("com.mysql.jdbc.Driver");

            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3308/game_database", "root","");

            Statement st = con.createStatement();

            ResultSet res2 = st.executeQuery("SELECT * FROM games_dates WHERE id_game = (SELECT max(id_game) FROM games_dates)");

            while(res2.next()){
                System.out.println("Date de la partie : " + res2.getDate(2));
                System.out.println("Heure du dÃ©but de la partie : " + res2.getTime(3));
                System.out.println("Heure de fin de la partie : " + res2.getTime(4));
            }

            return true;

        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            System.out.println();
        }
    }
}

