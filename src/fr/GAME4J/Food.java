package fr.GAME4J;

public class Food extends Case{

    public Nature nature;

    public Food(int coordinateX, int coordinateY, int number) {
        super(coordinateX, coordinateY, number);
        this.nature= Nature.FOOD;
    }

}
